#-*- coding: utf-8 -*-

import sys
import operator
import logging
import math
from collections import deque
logging.basicConfig(level=logging.DEBUG)
_logger = logging.getLogger(__name__)

OPERATORS = {
    '+': operator.add,
    '-': operator.sub,
    '/': lambda x, y: int(operator.truediv(x, y)),
    '*': operator.mul,
    'sqrt': lambda x: int(math.sqrt(x)),
    'MAX': max,
}

UNARIES = ('sqrt')
BINARIES = ('+', '-', '/', '*')
GLUES = ('MAX')

def rpn_operate(operation, stack):
    result = None
    operator_str = operation[0]
    operator_arity = operation[1]
    _logger.debug("operator {} with {} arguments".format(operator_str, operator_arity))
    
    #We need to degue the arguments used for the operation
    args_list = [stack.pop() for i in range(0, operator_arity)][::-1]
    _logger.debug("arguments list : {}".format(args_list))
    
    #Executing the operation, the parameters are used differently if the
    #operator is a binary, an unary or a glue one.
    if operator_str in BINARIES:
        result = OPERATORS[operator_str](args_list[0], args_list[1])
        _logger.debug("{} {} {} = {}".format(args_list[0], operator_str, args_list[1], result))
    elif operator_str in UNARIES:
        result = OPERATORS[operator_str](args_list[0])
        _logger.debug("{}({}) = {}".format(operator_str, args_list[0], result))
    elif operator_str in GLUES:
        result = OPERATORS[operator_str](args_list)
        _logger.debug("{}({}) = {}".format(operator_str, args_list, result))
    
    #Then we push the result of the operation in the stack
    stack.append(result)
    _logger.debug("stack : {}".format(stack))
    return stack

def cast_rpn(rpn_str):
    rpn = rpn_str.split(' ')
    glues_arity = 0 #counter used to know how many arguments will have to be
    #absorbed by a glue operator, should we meet one.

    for i in range(0, len(rpn)):
        if rpn[i] not in OPERATORS.keys():
            try:
                rpn[i] = int(rpn[i])
                glues_arity += 1
            except ValueError:
                raise ValueError("rpn should contain only integers or operators, found : {}".format(rpn[i]))
        else:
            if rpn[i] in BINARIES:        
                rpn[i] = (rpn[i], 2)
                #if we meet a binary operator, the stack will loose to items
                #and get a new one, being the result of the operation.
                glues_arity = glues_arity - 1 #so one less argument for
                #future glue operator, if there is any
            elif rpn[i] in UNARIES:
                rpn[i] = (rpn[i], 1)
                #we loose one argument, but we get a new one, being the result
                #of the operation.
                #So it does not impact the number of argument of a glue
                #operator
            elif rpn[i] in GLUES:
                rpn[i] = (rpn[i], glues_arity)
                #the glue operator absorbs all previous arguments, so the
                #counter has to be set back to 0.
                glues_arity = 0

    #A RPN should end with an operator
    if isinstance(rpn[-1], int):
        raise ValueError("Last RPN term must be an operator, found {}".format(rpn[-1]))
   
    return rpn

def compute_rpn(rpn):
    stack = deque()

    for i in range(0, len(rpn)):
        _logger.debug("element : {}".format(rpn[i]))
        if isinstance(rpn[i], int):
            stack.append(rpn[i])
            _logger.debug("appended in stack : {}".format(stack))
        else:
            stack = rpn_operate(rpn[i], stack)
    
    #In the end, the stack should contain only one item, being the result of
    #its evaluation
    if len(stack) != 1:
        raise Exception("Stack should contain one element, but contains {}. An error occur during the evaluation or the RPN is badly formated".format(stack))
    #Therefore, function returns the one last item of the stack
    return stack.pop()

if __name__ == '__main__':
    rpn_str = sys.argv[1]
    _logger.debug("rpn : {}".format(rpn_str))
    
    rpn = cast_rpn(rpn_str)
    _logger.debug("casted rpn : {}".format(rpn))

    result = compute_rpn(rpn)
    _logger.info("result = {}".format(result))
