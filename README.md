#RPN Calculator

The project was to create a python script able to evaluate a RPN expression and
to extend it to support the square-root (sqrt) operation and the 'max' (MAX)
operation.

The following samples were given :

20 5 /        => 20/5 = 4
4 2 + 3 -     => (4+2)-3 = 3
3 5 8 * 7 + * => ((5*8)+7)*3 = 141

Add the SQRT operation :

9 SQRT => √9 = 3

Add the MAX operation :

5 3 4 2 9 1 MAX   => MAX(5, 3, 4, 2, 9, 1) = 9
4 5 MAX 1 2 MAX * => MAX(4,5) * MAX(1,2) = 10

They have been implemented in the tests (with pytest), to check the behavior of the script.
