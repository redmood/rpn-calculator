#-*- coding: utf-8 -*-
import pytest
import rpn
from collections import deque

def test_cast_rpn_valid():
    rpn_str = '3 4 +'
    assert rpn.cast_rpn(rpn_str) == [3, 4, ('+', 2)]

def test_cast_rpn_int_error():
    with pytest.raises(ValueError) as e_info:
        rpn_str = '3.2 4 +'
        rpn.cast_rpn(rpn_str)

def test_cast_rpn_operator_error():
    with pytest.raises(ValueError) as e_info:
        rpn_str = '3 4 u'
        rpn.cast_rpn(rpn_str)

def test_cast_rpn_ending_error():
    with pytest.raises(ValueError) as e_info:
        rpn_str = '3 4 + 2'
        rpn.cast_rpn(rpn_str)

def test_rpn_operate_with_binary_operator():
    stack = deque([3, 2, 4])
    operation = ('+', 2)
    stack = rpn.rpn_operate(operation, stack)
    assert len(stack) == 2
    assert stack[0] == 3
    #stack[1] is supposed to be : 2 + 4 = 6
    assert stack[1] == 6

def test_rpn_operate_with_unary_operator():
    stack = deque([3, 4])
    operation = ('sqrt', 1)
    stack = rpn.rpn_operate(operation, stack)
    assert len(stack) == 2
    assert stack[0] == 3
    #stack[1] is supposed to be sqrt(4) = 2
    assert stack[1] == 2

def test_rpn_operate_with_glue_operator():
    stack = deque([16, 8, 4, 2])
    operation = ('MAX', 3)
    stack = rpn.rpn_operate(operation, stack)
    assert len(stack) == 2
    assert stack[0] == 16
    #stack[1] is supposed to be max(8, 4, 2) = 8
    assert stack[1] == 8

def test_compute_rpn_valid():
    rpn_res_tuples = list()
    rpn_res_tuples.append(([20, 5, ('/', 2)], 4))
    rpn_res_tuples.append(([4, 2, ('+', 2), 3, ('-', 2)], 3))
    rpn_res_tuples.append(([3, 5, 8, ('*', 2), 7, ('+', 2), ('*', 2)], 141))
    rpn_res_tuples.append(([9, ('sqrt', 1)], 3))
    rpn_res_tuples.append(([5, 3, 4, 2, 9, 1, ('MAX', 6)], 9))
    rpn_res_tuples.append(([4, 5, ('MAX', 2), 1, 2, ('MAX', 2), ('*', 2)], 10))

    for rpn_list, res in rpn_res_tuples:
        assert rpn.compute_rpn(rpn_list) == res

